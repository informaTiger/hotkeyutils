/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package hotkeyutils.gui;

import hotkeyutils.entity.Resource;
import hotkeyutils.entity.Hotkey;
import hotkeyutils.entity.Category;
import hotkeyutils.entity.*;
import hotkeyutils.entity.Process;
import hotkeyutils.entity.controller.*;
import java.awt.Color;
import java.awt.Toolkit;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import javafx.scene.input.KeyCode;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.filechooser.FileSystemView;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import org.jnativehook.GlobalScreen;
import org.jnativehook.NativeHookException;
import org.jnativehook.keyboard.NativeKeyEvent;
import org.jnativehook.keyboard.NativeKeyListener;

/**
 *
 * @author Thomas
 */
public class Mainframe extends javax.swing.JFrame implements NativeKeyListener {

    private HotkeyController hotkeyController = null;
    
    private ProcessController processController = null;
    
    private ResourceController resourceController = null;
    
    private CategoryController categoryController = null;
    
    private Category category = null;
    
    private ResourceType resourceType = null;
    
    private boolean active = true;
    
    private int mode = 1;
    
    private static final Logger logger = Logger.getLogger("hotkeyLogger");
    
    public Mainframe() {
        initComponents();
        this.setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("key.png")));
                
        LogManager.getLogManager().reset();
        FileHandler fileHandler = null;
        try {
            fileHandler = new FileHandler(System.getProperty("user.dir") + File.separatorChar + "debug.log", false);
            logger.addHandler(fileHandler);
            fileHandler.setFormatter(new SimpleFormatter());
        } catch (IOException | SecurityException ex) {
            logger.log(Level.SEVERE, ex.getMessage());
        }
        logger.setLevel(Level.ALL);
        
        logger.log(Level.INFO, "Run on: {0}", new Date());
        logger.log(Level.CONFIG, "Operating System is {0} - {1}; running on a {2} architecture.", new Object[]{System.getProperty("os.name"), System.getProperty("os.version"), System.getProperty("os.arch")});
        logger.log(Level.CONFIG, "Java version: {0}", System.getProperty("java.version"));
        
        Logger logger = Logger.getLogger(GlobalScreen.class.getPackage().getName());
        logger.setLevel(Level.OFF);
        
        categoryController = new CategoryController();
        hotkeyController = new HotkeyController();
        processController = new ProcessController();
        resourceController = new ResourceController();
        
        for (Category category : categoryController.getAll()){
            jCBCategory.addItem(category.getName());
        }
        
        for (KeyCode keyCode : KeyCode.values()){
            jCBKeys.addItem(keyCode.getName());
        }
        
        refreshUI();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTPContainer = new javax.swing.JTabbedPane();
        jPCurrentHotkey = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTHotkeys = new javax.swing.JTree();
        jPDetails = new javax.swing.JPanel();
        jLFilename = new javax.swing.JLabel();
        jLDirectory = new javax.swing.JLabel();
        jLFilesize = new javax.swing.JLabel();
        jLDateOfCreation = new javax.swing.JLabel();
        jLLastEditingDate = new javax.swing.JLabel();
        jLPermission = new javax.swing.JLabel();
        jLIcon = new javax.swing.JLabel();
        jBLaunch = new javax.swing.JButton();
        jLParameter = new javax.swing.JLabel();
        jPSetHotkey = new javax.swing.JPanel();
        jPContainer = new javax.swing.JPanel();
        jCBKeys = new javax.swing.JComboBox();
        jLKey = new javax.swing.JLabel();
        jLCategory = new javax.swing.JLabel();
        jCBCategory = new javax.swing.JComboBox();
        jLResource = new javax.swing.JLabel();
        jTFResource = new javax.swing.JTextField();
        jBChooseResource = new javax.swing.JButton();
        jLProgram = new javax.swing.JLabel();
        jTFProgram = new javax.swing.JTextField();
        jBChooseProgram = new javax.swing.JButton();
        jBOK = new javax.swing.JButton();
        jBCreateCategory = new javax.swing.JButton();
        jPConfiguration = new javax.swing.JPanel();
        jBDropAll = new javax.swing.JButton();
        jBEdit = new javax.swing.JButton();
        jBDelete = new javax.swing.JButton();
        jPHeader = new javax.swing.JPanel();
        jLEnabled = new javax.swing.JLabel();
        jLGeneralHotkeys = new javax.swing.JLabel();
        jLCurrentState = new javax.swing.JLabel();

        setTitle("HotkeyUtils");
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        jTPContainer.setRequestFocusEnabled(false);

        javax.swing.tree.DefaultMutableTreeNode treeNode1 = new javax.swing.tree.DefaultMutableTreeNode("Hotkeys");
        jTHotkeys.setModel(new javax.swing.tree.DefaultTreeModel(treeNode1));
        jTHotkeys.setAutoscrolls(true);
        jTHotkeys.addTreeSelectionListener(new javax.swing.event.TreeSelectionListener() {
            public void valueChanged(javax.swing.event.TreeSelectionEvent evt) {
                jTHotkeysValueChanged(evt);
            }
        });
        jScrollPane1.setViewportView(jTHotkeys);

        jPDetails.setBorder(javax.swing.BorderFactory.createTitledBorder("Details"));

        jLFilename.setText("Filename:");

        jLDirectory.setText("Directory:");

        jLFilesize.setText("Filesize:");

        jLDateOfCreation.setText("Date of creation:");

        jLLastEditingDate.setText("Last editing date:");

        jLPermission.setText("Permission:");

        jLIcon.setToolTipText("");

        jBLaunch.setText("Launch");
        jBLaunch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBLaunchActionPerformed(evt);
            }
        });

        jLParameter.setText("Parameter:");

        javax.swing.GroupLayout jPDetailsLayout = new javax.swing.GroupLayout(jPDetails);
        jPDetails.setLayout(jPDetailsLayout);
        jPDetailsLayout.setHorizontalGroup(
            jPDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPDetailsLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPDetailsLayout.createSequentialGroup()
                        .addGroup(jPDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLFilesize)
                            .addComponent(jLDateOfCreation)
                            .addComponent(jLLastEditingDate)
                            .addComponent(jLPermission))
                        .addContainerGap(233, Short.MAX_VALUE))
                    .addGroup(jPDetailsLayout.createSequentialGroup()
                        .addGroup(jPDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPDetailsLayout.createSequentialGroup()
                                .addComponent(jLFilename)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLIcon, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jLDirectory))
                        .addGap(19, 19, 19))
                    .addGroup(jPDetailsLayout.createSequentialGroup()
                        .addGroup(jPDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPDetailsLayout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(jBLaunch))
                            .addGroup(jPDetailsLayout.createSequentialGroup()
                                .addComponent(jLParameter)
                                .addGap(0, 0, Short.MAX_VALUE)))
                        .addContainerGap())))
        );
        jPDetailsLayout.setVerticalGroup(
            jPDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPDetailsLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLFilename)
                    .addComponent(jLIcon, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLDirectory)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLFilesize)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLDateOfCreation)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLLastEditingDate)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLPermission)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLParameter)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jBLaunch)
                .addContainerGap())
        );

        javax.swing.GroupLayout jPCurrentHotkeyLayout = new javax.swing.GroupLayout(jPCurrentHotkey);
        jPCurrentHotkey.setLayout(jPCurrentHotkeyLayout);
        jPCurrentHotkeyLayout.setHorizontalGroup(
            jPCurrentHotkeyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPCurrentHotkeyLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 191, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPDetails, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPCurrentHotkeyLayout.setVerticalGroup(
            jPCurrentHotkeyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPCurrentHotkeyLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPCurrentHotkeyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 247, Short.MAX_VALUE)
                    .addComponent(jPDetails, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        jTPContainer.addTab("Current Hotkey", new javax.swing.ImageIcon(getClass().getResource("/hotkeyutils/gui/key_small.png")), jPCurrentHotkey, ""); // NOI18N

        jPContainer.setBorder(javax.swing.BorderFactory.createEtchedBorder(java.awt.Color.white, null));

        jLKey.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLKey.setText("Key");

        jLCategory.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLCategory.setText("Category");

        jLResource.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLResource.setText("Resource");

        jTFResource.setEditable(false);

        jBChooseResource.setText("...");
        jBChooseResource.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBChooseResourceActionPerformed(evt);
            }
        });

        jLProgram.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLProgram.setText("Program");

        jBChooseProgram.setText("...");
        jBChooseProgram.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBChooseProgramActionPerformed(evt);
            }
        });

        jBOK.setText("Apply");
        jBOK.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBOKActionPerformed(evt);
            }
        });

        jBCreateCategory.setText("+");
        jBCreateCategory.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBCreateCategoryActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPContainerLayout = new javax.swing.GroupLayout(jPContainer);
        jPContainer.setLayout(jPContainerLayout);
        jPContainerLayout.setHorizontalGroup(
            jPContainerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPContainerLayout.createSequentialGroup()
                .addGroup(jPContainerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPContainerLayout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jBOK))
                    .addGroup(jPContainerLayout.createSequentialGroup()
                        .addGap(25, 25, 25)
                        .addGroup(jPContainerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPContainerLayout.createSequentialGroup()
                                .addComponent(jLProgram)
                                .addGap(34, 34, 34)
                                .addComponent(jTFProgram, javax.swing.GroupLayout.PREFERRED_SIZE, 163, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jBChooseProgram, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(jPContainerLayout.createSequentialGroup()
                                .addGroup(jPContainerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLKey)
                                    .addComponent(jLCategory)
                                    .addComponent(jLResource))
                                .addGap(30, 30, 30)
                                .addGroup(jPContainerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jCBKeys, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addGroup(jPContainerLayout.createSequentialGroup()
                                        .addGroup(jPContainerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addComponent(jCBCategory, 0, 163, Short.MAX_VALUE)
                                            .addComponent(jTFResource))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(jPContainerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jBChooseResource, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(jBCreateCategory, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))))))
                .addGap(32, 32, 32))
        );
        jPContainerLayout.setVerticalGroup(
            jPContainerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPContainerLayout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(jPContainerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLKey)
                    .addComponent(jCBKeys, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20)
                .addGroup(jPContainerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLCategory)
                    .addGroup(jPContainerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jCBCategory, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jBCreateCategory)))
                .addGap(19, 19, 19)
                .addGroup(jPContainerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLResource)
                    .addComponent(jTFResource, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jBChooseResource))
                .addGap(18, 18, 18)
                .addGroup(jPContainerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLProgram)
                    .addComponent(jTFProgram, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jBChooseProgram))
                .addGap(30, 30, 30)
                .addComponent(jBOK)
                .addGap(24, 24, 24))
        );

        jPConfiguration.setBorder(javax.swing.BorderFactory.createTitledBorder("Configuration"));

        jBDropAll.setIcon(new javax.swing.ImageIcon(getClass().getResource("/hotkeyutils/gui/bomb.png"))); // NOI18N
        jBDropAll.setText("Drop all hotkeys");
        jBDropAll.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBDropAllActionPerformed(evt);
            }
        });

        jBEdit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/hotkeyutils/gui/edit.png"))); // NOI18N
        jBEdit.setText("Edit hotkey");
        jBEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBEditActionPerformed(evt);
            }
        });

        jBDelete.setIcon(new javax.swing.ImageIcon(getClass().getResource("/hotkeyutils/gui/erase.png"))); // NOI18N
        jBDelete.setText("Delete hotkey");
        jBDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBDeleteActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPConfigurationLayout = new javax.swing.GroupLayout(jPConfiguration);
        jPConfiguration.setLayout(jPConfigurationLayout);
        jPConfigurationLayout.setHorizontalGroup(
            jPConfigurationLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPConfigurationLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPConfigurationLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jBEdit, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jBDelete, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jBDropAll, javax.swing.GroupLayout.DEFAULT_SIZE, 145, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPConfigurationLayout.setVerticalGroup(
            jPConfigurationLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPConfigurationLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jBDropAll)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jBDelete)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jBEdit)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPSetHotkeyLayout = new javax.swing.GroupLayout(jPSetHotkey);
        jPSetHotkey.setLayout(jPSetHotkeyLayout);
        jPSetHotkeyLayout.setHorizontalGroup(
            jPSetHotkeyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPSetHotkeyLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPContainer, javax.swing.GroupLayout.PREFERRED_SIZE, 357, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPConfiguration, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPSetHotkeyLayout.setVerticalGroup(
            jPSetHotkeyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPSetHotkeyLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPSetHotkeyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPContainer, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPConfiguration, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        jTPContainer.addTab("Set Hotkey", new javax.swing.ImageIcon(getClass().getResource("/hotkeyutils/gui/advancedsettings.png")), jPSetHotkey); // NOI18N

        jPHeader.setBackground(new java.awt.Color(191, 10, 0));

        jLEnabled.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLEnabled.setForeground(new java.awt.Color(255, 255, 255));
        jLEnabled.setText("Hotkeys enabled [F2]:");

        jLGeneralHotkeys.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLGeneralHotkeys.setForeground(new java.awt.Color(255, 255, 255));
        jLGeneralHotkeys.setText("Show/Hide [F4]       Exit [F6]");

        jLCurrentState.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLCurrentState.setForeground(new java.awt.Color(19, 63, 22));
        jLCurrentState.setText("true");

        javax.swing.GroupLayout jPHeaderLayout = new javax.swing.GroupLayout(jPHeader);
        jPHeader.setLayout(jPHeaderLayout);
        jPHeaderLayout.setHorizontalGroup(
            jPHeaderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPHeaderLayout.createSequentialGroup()
                .addGap(26, 26, 26)
                .addComponent(jLEnabled)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLCurrentState)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLGeneralHotkeys)
                .addGap(32, 32, 32))
        );
        jPHeaderLayout.setVerticalGroup(
            jPHeaderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPHeaderLayout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addGroup(jPHeaderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLEnabled)
                    .addComponent(jLGeneralHotkeys)
                    .addComponent(jLCurrentState))
                .addContainerGap(32, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPHeader, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jTPContainer)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(jPHeader, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTPContainer))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jBChooseResourceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBChooseResourceActionPerformed
        ResourceDialog resourceDialog = new ResourceDialog(this, true);
        if (resourceDialog.showDialog() == 0){
            if (resourceDialog.getResourceType() == ResourceType.FOLDER)
                jTFResource.setText(resourceDialog.getFolder());
            else if (resourceDialog.getResourceType() == ResourceType.URL)
                jTFResource.setText(resourceDialog.getUrl());
            else
                jTFResource.setText(resourceDialog.getParameter());
            resourceType = resourceDialog.getResourceType();
        }
    }//GEN-LAST:event_jBChooseResourceActionPerformed

    private void jBChooseProgramActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBChooseProgramActionPerformed
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setFileFilter(new FileNameExtensionFilter("Executables (*.app, *.exe, *.com, *.jar)", new String[]{ "app", "exe", "com", "jar" }));
        if (fileChooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION){
            jTFProgram.setText(fileChooser.getSelectedFile().getAbsolutePath());
        }
    }//GEN-LAST:event_jBChooseProgramActionPerformed

    private void jBCreateCategoryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBCreateCategoryActionPerformed
        CategoryDialog categoryDialog = new CategoryDialog(this, true);
        if (categoryDialog.showDialog() == 0){
            category = new Category();
            category.setName(categoryDialog.getName());
            category.setKeycode(categoryDialog.getKey());
            jCBCategory.addItem(categoryDialog.getName());
        }
    }//GEN-LAST:event_jBCreateCategoryActionPerformed

    private void refreshUI(){
        DefaultTreeModel defaultTreeModel = (DefaultTreeModel)jTHotkeys.getModel();
        DefaultMutableTreeNode rootNode = (DefaultMutableTreeNode) defaultTreeModel.getRoot();
        rootNode.removeAllChildren();
        
        for (Category category : categoryController.getAll()){
            DefaultMutableTreeNode children = new DefaultMutableTreeNode(category.getName());
            for (Hotkey hotkey : category.getHotkeys()){
                children.add(new DefaultMutableTreeNode(hotkey.getName()));
            }
            rootNode.add(children);
        }
        defaultTreeModel.reload();
        
        for (int i = 0; i < jTHotkeys.getRowCount(); i++){
            jTHotkeys.expandRow(i);
        }
        
        if (jTHotkeys.getRowCount() > 2){
            jTHotkeys.setSelectionRow(2);
        }
        
        jCBCategory.removeAllItems();
        for (Category category : categoryController.getAll()){
            jCBCategory.addItem(category.getName());
        }
        clearAll();
    }
    
    private void clearAll(){
        jTFProgram.setText(null);
        jTFResource.setText(null);
        if (jCBKeys.getItemCount() > 0)
            jCBKeys.setSelectedIndex(0);
        if (jCBCategory.getItemCount() > 0)
            jCBCategory.setSelectedIndex(0);
        resourceType = null;
        category = null;
    }
    
    private Hotkey getSelectedHotkey(){
        DefaultMutableTreeNode selectedNode;
        try {
          selectedNode = (DefaultMutableTreeNode)jTHotkeys.getSelectionPath().getLastPathComponent();
        } catch (NullPointerException ex){
            logger.log(Level.WARNING, "Hotkey undefined :{0}", ex.getMessage());
            return null;
        }
        DefaultMutableTreeNode parent = (DefaultMutableTreeNode)selectedNode.getParent();
        return hotkeyController.getByCategory(selectedNode.getUserObject().toString(), parent.getUserObject().toString());
    }
    
    private void jBOKActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBOKActionPerformed
        if (jCBKeys.getSelectedItem() != null && jCBCategory.getSelectedItem() != null && jTFProgram.getText() != null && jTFResource.getText() != null){
            Process process = new Process(jTFProgram.getText());
            Resource resource = new Resource(jTFResource.getText(), resourceType);
            resource.addProcess(process);
            process.setResource(resource);
            Hotkey hotkey = new Hotkey(jCBKeys.getSelectedItem().toString(), process);
            List<Hotkey> hotkeyList = new ArrayList<Hotkey>();
            hotkeyList.add(hotkey);
            
            if (category == null){
                category = categoryController.getByName(jCBCategory.getSelectedItem().toString());
                category.addHotkeys(hotkeyList);
            }
            else {
                category.setHotkeys(hotkeyList);
            }
            hotkey.setCategory(category);
            
            processController.insert(process);
            hotkeyController.insert(hotkey);
            JOptionPane.showMessageDialog(this, "Hotkey has been saved.", "Information", JOptionPane.INFORMATION_MESSAGE);
            refreshUI();
        }
        else {
            JOptionPane.showMessageDialog(this, "All fields must be filled.", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_jBOKActionPerformed

    private void jTHotkeysValueChanged(javax.swing.event.TreeSelectionEvent evt) {//GEN-FIRST:event_jTHotkeysValueChanged
        Hotkey hotkey = this.getSelectedHotkey();
        if (hotkey == null)
            return;
        File file = new File(hotkey.getProcess().getPath());
        BasicFileAttributes attributes = null;
        try {
            attributes = Files.readAttributes(Paths.get(hotkey.getProcess().getPath()), BasicFileAttributes.class);
        } catch (IOException ex) {
            logger.log(Level.SEVERE, "Reading attributes failed: {0}", ex.getMessage());
            return;
        }
        jLFilename.setText("Filename: " + file.getName());
        if (file.getParent().length() > 33)
            jLDirectory.setText("Directory: " + file.getParent().substring(0, 33) + "...");
        else
            jLDirectory.setText("Directory: " + file.getParent());
        jLFilesize.setText("Filesize: " + file.length() / 1024 + " KB");
        jLDateOfCreation.setText("Date of creation: " + new Date(attributes.creationTime().toMillis()));
        jLLastEditingDate.setText("Last editing date: " + new Date(attributes.lastModifiedTime().toMillis()));
        jLPermission.setText("Permission: [r=" + (file.canRead() ? 1 : 0) + "], [w=" + (file.canWrite() ? 1 : 0) + "], [e=" + (file.canExecute() ? 1 : 0) + "]");
        jLIcon.setIcon(FileSystemView.getFileSystemView().getSystemIcon(file));
        if (hotkey.getProcess().getResource().getString().length() > 30)
            jLParameter.setText("Parameter: " + hotkey.getProcess().getResource().getString().substring(0, 30) + "..." + " (" + hotkey.getProcess().getResource().getType() +")");
        else
            jLParameter.setText("Parameter: " + hotkey.getProcess().getResource().getString() + " (" + hotkey.getProcess().getResource().getType() +")");
    }//GEN-LAST:event_jTHotkeysValueChanged

    private void jBDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBDeleteActionPerformed
        DeleteDialog deleteDialog = new DeleteDialog(this, true);
        if (deleteDialog.showDialog() == 0){
            if (JOptionPane.showConfirmDialog(this, "Are you sure to delete the hotkey " + deleteDialog.getHotkey().getName() + " from the category " + deleteDialog.getCategory().getName() + "?", "Question", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE) == 0){
                if (deleteDialog.getCategory().getHotkeys().size() <= 1)
                    categoryController.remove(deleteDialog.getCategory());
                hotkeyController.remove(deleteDialog.getHotkey());
                refreshUI();
            }
        }
    }//GEN-LAST:event_jBDeleteActionPerformed

    private void jBLaunchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBLaunchActionPerformed
        Hotkey hotkey = this.getSelectedHotkey();
        if (hotkey == null)
            return;

        try {
            new ProcessBuilder(hotkey.getProcess().getPath(), hotkey.getProcess().getResource().getString()).start();
        } catch (IOException ex) {
            logger.log(Level.SEVERE, "Process could not be build: {0}", ex.getMessage());
        }
    }//GEN-LAST:event_jBLaunchActionPerformed

    private void jBDropAllActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBDropAllActionPerformed
        if (JOptionPane.showConfirmDialog(this, "Are you sure to drop all hotkeys?", "Question", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE) == 0) {
            hotkeyController.clear();
            categoryController.clear();
            processController.clear();
            resourceController.clear();
            refreshUI();
        }
    }//GEN-LAST:event_jBDropAllActionPerformed

    private void jBEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBEditActionPerformed
        EditDialog editDialog = new EditDialog(this, true);
        if (editDialog.showDialog() == 0){
            if(editDialog.getHotkey() != null && editDialog.getResource() != null && editDialog.getProcess() != null){
                int updated = 0;
                Process process = editDialog.getProcess();
                Resource resource = editDialog.getResource();
                resource.addProcess(process);
                process.setResource(resource);
                process.setId(editDialog.getHotkey().getProcess().getId());
                resource.setId(editDialog.getHotkey().getProcess().getResource().getId());
                
                updated = processController.update(process);
                updated += resourceController.update(resource);
                JOptionPane.showMessageDialog(this, updated + " Entries have been updated.", "Information", JOptionPane.INFORMATION_MESSAGE);
            } else {
              JOptionPane.showMessageDialog(this, "All fields must be filled.", "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }//GEN-LAST:event_jBEditActionPerformed

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        try {
            GlobalScreen.registerNativeHook();
        } catch (NativeHookException ex) {
            logger.log(Level.SEVERE, "Native Hook could not be registered: {0}", ex.getMessage());
        }
        GlobalScreen.getInstance().addNativeKeyListener(this);
    }//GEN-LAST:event_formWindowOpened

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        // TODO add your handling code here:
        try {
            GlobalScreen.unregisterNativeHook();
        } catch (Exception ex){
            logger.log(Level.SEVERE, "Native Hook could not be unregistered: {0}", ex.getMessage());
        }
        System.runFinalization();
        System.exit(0);
    }//GEN-LAST:event_formWindowClosing

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jBChooseProgram;
    private javax.swing.JButton jBChooseResource;
    private javax.swing.JButton jBCreateCategory;
    private javax.swing.JButton jBDelete;
    private javax.swing.JButton jBDropAll;
    private javax.swing.JButton jBEdit;
    private javax.swing.JButton jBLaunch;
    private javax.swing.JButton jBOK;
    private javax.swing.JComboBox jCBCategory;
    private javax.swing.JComboBox jCBKeys;
    private javax.swing.JLabel jLCategory;
    private javax.swing.JLabel jLCurrentState;
    private javax.swing.JLabel jLDateOfCreation;
    private javax.swing.JLabel jLDirectory;
    private javax.swing.JLabel jLEnabled;
    private javax.swing.JLabel jLFilename;
    private javax.swing.JLabel jLFilesize;
    private javax.swing.JLabel jLGeneralHotkeys;
    private javax.swing.JLabel jLIcon;
    private javax.swing.JLabel jLKey;
    private javax.swing.JLabel jLLastEditingDate;
    private javax.swing.JLabel jLParameter;
    private javax.swing.JLabel jLPermission;
    private javax.swing.JLabel jLProgram;
    private javax.swing.JLabel jLResource;
    private javax.swing.JPanel jPConfiguration;
    private javax.swing.JPanel jPContainer;
    private javax.swing.JPanel jPCurrentHotkey;
    private javax.swing.JPanel jPDetails;
    private javax.swing.JPanel jPHeader;
    private javax.swing.JPanel jPSetHotkey;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField jTFProgram;
    private javax.swing.JTextField jTFResource;
    private javax.swing.JTree jTHotkeys;
    private javax.swing.JTabbedPane jTPContainer;
    // End of variables declaration//GEN-END:variables

    @Override
    public void nativeKeyPressed(NativeKeyEvent nke) {
        /* Unimplemented */
    }

    @Override
    public void nativeKeyReleased(NativeKeyEvent nke) {
        if (nke.getKeyCode() == NativeKeyEvent.VC_F2 /* 68; VC_F2 */) {
            active = !active;
            jLCurrentState.setText(Boolean.toString(active));
            if (active)
                jLCurrentState.setForeground(new Color(19, 63, 22));
            else
                jLCurrentState.setForeground(new Color(140, 33, 0));
            return;
        }
        else if (nke.getKeyCode() == NativeKeyEvent.VC_F4 /* 70; VC_F4 */) {
            this.setVisible(!this.isVisible());
            return;
        }
        else if (nke.getKeyCode() == NativeKeyEvent.VC_F6 /* 72; VC_F6 */){
            System.exit(0);
        }
        
        if (active) {
            switch(nke.getKeyCode()){
                case NativeKeyEvent.VC_9: /* 18; VC_9 */
                case NativeKeyEvent.VC_8: /* 17; VC_8 */
                case NativeKeyEvent.VC_7: /* 16; VC_7 */
                case NativeKeyEvent.VC_6: /* 15; VC_6 */
                case NativeKeyEvent.VC_5: /* 14; VC_5 */
                case NativeKeyEvent.VC_4: /* 13; VC_4 */
                case NativeKeyEvent.VC_3: /* 12; VC_3 */
                case NativeKeyEvent.VC_2: /* 11; VC_2 */
                case NativeKeyEvent.VC_1: /* 10; VC_1 */
                case NativeKeyEvent.VC_0: /* 19; VC_0 */ {
                    mode = Integer.valueOf(NativeKeyEvent.getKeyText(nke.getKeyCode()));
                    break;
                }
                default: {
                    Hotkey hotkey = hotkeyController.getByCategory(NativeKeyEvent.getKeyText(nke.getKeyCode()), categoryController.getByKey(mode).getName());
                    if (hotkey == null)
                        return;
                    
                    try {
                        new ProcessBuilder(hotkey.getProcess().getPath(), hotkey.getProcess().getResource().getString()).start();
                    } catch (IOException ex) {
                        logger.log(Level.SEVERE, "Process could not be build: {0}", ex.getMessage());
                    }
                    break;
                }
            }
        }
    }

    @Override
    public void nativeKeyTyped(NativeKeyEvent nke) {
        /* Unimplemented */
    }
}
