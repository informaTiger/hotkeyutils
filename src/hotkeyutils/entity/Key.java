package hotkeyutils.entity;


import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity(name="key")
@Table(name="key")
@NamedQueries({@NamedQuery(name="Key.getAll",query="SELECT k FROM key k"),@NamedQuery(name="Key.getAvailable",query="SELECT k FROM key k WHERE k.key NOT IN (SELECT c.keycode FROM category c)")})
public class Key implements Serializable {

    @Column(unique=true,nullable=false)
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int id;
    @Basic
    private int key;

    public Key() {

    }
    
    public Key(int key){
        this.key = key;
    }
   
    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }
   
    public int getKey() {
        return this.key;
    }

    public void setKey(int key) {
        this.key = key;
    }
}
