package hotkeyutils.entity;


import java.io.Serializable;
import java.util.ArrayList;

import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity(name="category")
@Table(name="category")
@NamedQueries({@NamedQuery(name="Category.getAll",query="SELECT c FROM category c"),@NamedQuery(name="Category.getByKey",query="SELECT c FROM category c WHERE c.keycode = :catKeycode"),@NamedQuery(name="Category.getByName",query="SELECT c FROM category c WHERE c.name = :catName")})
public  class Category implements Serializable {


    @Column(unique=true,nullable=false)
    @Id
    private int keycode;


    @OneToMany(cascade={CascadeType.REMOVE},targetEntity=Hotkey.class,mappedBy="category")
    private Collection<Hotkey> hotkeys;


    @Basic
    private String name;

    public Category(){
        this.hotkeys = new ArrayList();
    }


   public int getKeycode() {
        return this.keycode;
    }


  public void setKeycode (int keycode) {
        this.keycode = keycode;
    }



   public Collection<Hotkey> getHotkeys() {
        return this.hotkeys;
    }


  public void setHotkeys (Collection<Hotkey> hotkeys) {
        this.hotkeys = hotkeys;
    }

    public void addHotkeys(Collection<Hotkey> hotkeys) {
        this.hotkeys.addAll(hotkeys);
    }

   public String getName() {
        return this.name;
    }


  public void setName (String name) {
        this.name = name;
    }

}

