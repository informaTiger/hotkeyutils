package hotkeyutils.entity;


import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity(name="process")
@Table(name="process")
@NamedQueries({@NamedQuery(name="Process.getAll",query="SELECT p FROM process p"),@NamedQuery(name="Process.getByFilename",query="SELECT p FROM process p WHERE p.path LIKE :fileName"),@NamedQuery(name="Process.getByResource",query="SELECT p FROM process p JOIN p.resource r WHERE r.string = :resString"),@NamedQuery(name="Process.update",query="UPDATE process p SET p.path = :pPath WHERE p.id = :pId")})
public  class Process implements Serializable {


    @Basic
    private String path;


    @ManyToOne(cascade={CascadeType.PERSIST,CascadeType.REMOVE},targetEntity=Resource.class)
    private Resource resource;


    @Column(unique=true,nullable=false)
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int id;

    public Process(){

    }

    public Process(String path){
        this.path = path;
    }

   public String getPath() {
        return this.path;
    }


  public void setPath (String path) {
        this.path = path;
    }



   public Resource getResource() {
        return this.resource;
    }


  public void setResource (Resource resource) {
        this.resource = resource;
    }



   public int getId() {
        return this.id;
    }


  public void setId (int id) {
        this.id = id;
    }

}

