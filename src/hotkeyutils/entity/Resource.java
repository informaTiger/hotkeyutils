package hotkeyutils.entity;


import java.io.Serializable;

import hotkeyutils.entity.ResourceType;
import java.util.ArrayList;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity(name="resource")
@Table(name="resource")
@NamedQueries({@NamedQuery(name="Resource.getAll",query="SELECT r FROM resource r"),@NamedQuery(name="Resource.getByType",query="SELECT r FROM resource r WHERE r.type = :resType"),@NamedQuery(name="Resource.update",query="UPDATE resource r SET r.string = :resString , r.type = :resType WHERE r.id = :resId")})
public  class Resource implements Serializable {


    @OneToMany(cascade={CascadeType.REMOVE},targetEntity=Process.class,mappedBy="resource")
    private Collection<Process> processes;


    @Basic
    private String string;


    @Column(unique=true,nullable=false)
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int id;


    @Basic
    @Enumerated(EnumType.STRING)
    private ResourceType type;

    public Resource(){

    }
    
    public Resource(String string, ResourceType type){
        this.string = string;
        this.type = type;
        this.processes = new ArrayList();
    }


   public Collection<Process> getProcesses() {
        return this.processes;
    }


  public void setProcesses (Collection<Process> processes) {
        this.processes = processes;
    }

    public void addProcess(Process process){
        this.processes.add(process);
    }

   public String getString() {
        return this.string;
    }


  public void setString (String string) {
        this.string = string;
    }



   public int getId() {
        return this.id;
    }


  public void setId (int id) {
        this.id = id;
    }



   public ResourceType getType() {
        return this.type;
    }


  public void setType (ResourceType type) {
        this.type = type;
    }

}

