/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package hotkeyutils.entity.controller;

import hotkeyutils.entity.Resource;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

/**
 *
 * @author Thomas
 */
public class ResourceController {
    
    private EntityManager entityManager = null;
    private EntityManagerFactory entityManagerFactory = null;
    private static final Logger logger = Logger.getLogger("hotkeyLogger");
    
    public ResourceController(){
        if (entityManager == null){
            entityManagerFactory = Persistence.createEntityManagerFactory("hotkey");
            entityManager = entityManagerFactory.createEntityManager();
            logger.log(Level.INFO, "{0}: Entity Manager created;", this.getClass().getName());
        }
    }
    
    public List<Resource> getAll(){
        TypedQuery<Resource> query = entityManager.createNamedQuery("Resource.getAll", Resource.class);
        logger.log(Level.INFO, "getAll(): Returning {0} values;", query.getResultList().size());
        return query.getResultList();
    }
    
    public Resource getByType(String type){
        Query query = entityManager.createNamedQuery("Ressource.getByType").setParameter("resType", type);
        logger.log(Level.INFO, "getByType({0}): was called;", type);
        return (Resource) query.getSingleResult();
    }
    
    public void insert(Resource resource){
        entityManager.getTransaction().begin();
        entityManager.persist(resource);
        entityManager.getTransaction().commit();
        logger.log(Level.INFO, "insert(@Resource([Id={0}],[String={1}],[Type={2}])): was called;", new Object[]{resource.getId(), resource.getString(), resource.getType()});
    }
    
    public int update(Resource resource){
        entityManager.getTransaction().begin();
        int updated = entityManager.createNamedQuery("Resource.update").setParameter("resString", resource.getString())
                    .setParameter("resType", resource.getType())
                    .setParameter("resId", resource.getId()).executeUpdate();
        entityManager.getTransaction().commit();
        logger.log(Level.INFO, "update(@Resource([Id={0}],[String={1}],[Type={2}])): was called;", new Object[]{resource.getId(), resource.getString(), resource.getType()});
        return updated;
    }
    
    public int clear(){
        entityManager.getTransaction().begin();
        int deleted = entityManager.createQuery("DELETE FROM resource").executeUpdate();
        entityManager.createNativeQuery("ALTER TABLE resource ALTER COLUMN id RESTART WITH 1").executeUpdate();
        entityManager.getTransaction().commit();
        logger.log(Level.INFO, "clear(): dropped all resources;");
        return deleted;
    }
}
