/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hotkeyutils.entity.controller;

import hotkeyutils.entity.Key;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

/**
 *
 * @author Thomas
 */
public class KeyController {
    
    private EntityManager entityManager = null;
    private EntityManagerFactory entityManagerFactory = null;
    private static final Logger logger = Logger.getLogger("hotkeyLogger");
    
    public KeyController(){
        if (entityManager == null){
            entityManagerFactory = Persistence.createEntityManagerFactory("hotkey");
            entityManager = entityManagerFactory.createEntityManager();
            logger.log(Level.INFO, "{0}: Entity Manager created;", this.getClass().getName());
            if (getAll().isEmpty()){
                for (int i = 1; i < 10; i++) {
                    entityManager.getTransaction().begin();
                    entityManager.persist(new Key(i));
                    entityManager.getTransaction().commit();
                }
            }
        }
    }
    
    public List<Key> getAll(){
        TypedQuery<Key> query = entityManager.createNamedQuery("Key.getAll", Key.class);
        logger.log(Level.INFO, "getAll(): Returning {0} values;", query.getResultList().size());
        return query.getResultList();
    }
    
    public List<Key> getAvailable(){
        TypedQuery<Key> query = entityManager.createNamedQuery("Key.getAvailable", Key.class);
        logger.log(Level.INFO, "getAvailable(): was called;");
        return query.getResultList();
    }
}
