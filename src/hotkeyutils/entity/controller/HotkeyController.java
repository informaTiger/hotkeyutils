/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package hotkeyutils.entity.controller;

import hotkeyutils.entity.Hotkey;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

/**
 *
 * @author Thomas
 */
public class HotkeyController {
    
    private EntityManager entityManager = null;
    private EntityManagerFactory entityManagerFactory = null;
    private static final Logger logger = Logger.getLogger("hotkeyLogger");
    
    public HotkeyController(){
        if (entityManager == null){
            entityManagerFactory = Persistence.createEntityManagerFactory("hotkey");
            entityManager = entityManagerFactory.createEntityManager();
            logger.log(Level.INFO, "{0}: Entity Manager created;", this.getClass().getName());
        }
    }
    
    public List<Hotkey> getAll(){
        TypedQuery<Hotkey> query = entityManager.createNamedQuery("Hotkey.getAll", Hotkey.class);
        logger.log(Level.INFO, "getAll(): Returning {0} values;", query.getResultList().size());
        return query.getResultList();
    }
    
    public Hotkey getByCategory(String hotkeyName, String categoryName){
        Query query = entityManager.createNamedQuery("Hotkey.getByCategory").setParameter("hkName", hotkeyName).setParameter("catName", categoryName);
        logger.log(Level.INFO, "getByCategory(@String([hotkeyName={0}],[categoryName={1}])): was called;", new Object[]{hotkeyName, categoryName});
        return (Hotkey) query.getSingleResult();
    }
    
    public void insert(Hotkey hotkey){
        entityManager.getTransaction().begin();
        hotkey = entityManager.merge(hotkey);
        entityManager.persist(hotkey);
        entityManager.getTransaction().commit();
        logger.log(Level.INFO, "insert(@Hotkey([Id={0}],[Name={1}])): was called;", new Object[]{hotkey.getId(), hotkey.getName()});
    }
    
    public void remove(Hotkey hotkey){
        entityManager.getTransaction().begin();
        hotkey = entityManager.merge(hotkey);
        entityManager.remove(hotkey);
        entityManager.getTransaction().commit();
        logger.log(Level.INFO, "remove(@Hotkey([Id={0}],[Name={1}])): was called;", new Object[]{hotkey.getId(), hotkey.getName()});
    }
    
    public int clear(){
        entityManager.getTransaction().begin();
        int deleted = entityManager.createQuery("DELETE FROM hotkey").executeUpdate();
        entityManager.createNativeQuery("ALTER TABLE hotkey ALTER COLUMN id RESTART WITH 1").executeUpdate();
        entityManager.getTransaction().commit();
        logger.log(Level.INFO, "clear(): dropped all hotkeys;");
        return deleted;
    }
}
