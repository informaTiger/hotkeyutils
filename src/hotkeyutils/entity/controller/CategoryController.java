/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package hotkeyutils.entity.controller;
import hotkeyutils.entity.Category;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

/**
 *
 * @author Thomas
 */
public class CategoryController {
    
    private EntityManager entityManager = null;
    private EntityManagerFactory entityManagerFactory = null;
    private static final Logger logger = Logger.getLogger("hotkeyLogger");
    
    public CategoryController(){
        if (entityManager == null){
            entityManagerFactory = Persistence.createEntityManagerFactory("hotkey");
            entityManager = entityManagerFactory.createEntityManager();
            logger.log(Level.INFO, "{0}: Entity Manager created;", this.getClass().getName());
        }
    }
    
    public List<Category> getAll(){
        TypedQuery<Category> query = entityManager.createNamedQuery("Category.getAll", Category.class);
        logger.log(Level.INFO, "getAll(): Returning {0} values;", query.getResultList().size());
        return query.getResultList();
    }
    
    public Category getByKey(int keycode){
        Query query = entityManager.createNamedQuery("Category.getByKey").setParameter("catKeycode", keycode);
        logger.log(Level.INFO, "getByKey({0}): was called;", keycode);
        return (Category) query.getSingleResult();
    }
    
    public Category getByName(String name){
        Query query = entityManager.createNamedQuery("Category.getByName").setParameter("catName", name);
        logger.log(Level.INFO, "getByName({0}): was called;", name);
        return (Category) query.getSingleResult();
    }
    
    public void insert(Category category){
        entityManager.getTransaction().begin();
        category = entityManager.merge(category);
        entityManager.persist(category);
        entityManager.getTransaction().commit();
        logger.log(Level.INFO, "insert(@Category([Keycode={0}],[Name={1}])): was called;", new Object[]{category.getKeycode(), category.getName()});
    }
    
    public void remove(Category category){
        entityManager.getTransaction().begin();
        category = entityManager.merge(category);
        entityManager.remove(category);
        entityManager.getTransaction().commit();
        logger.log(Level.INFO, "remove(@Category([Keycode={0}],[Name={1}])): was called;", new Object[]{category.getKeycode(), category.getName()});
    }
    
    public int clear(){
        entityManager.getTransaction().begin();
        int deleted = entityManager.createQuery("DELETE FROM category").executeUpdate();
        entityManager.createNativeQuery("ALTER TABLE category ALTER COLUMN id RESTART WITH 1").executeUpdate();
        entityManager.getTransaction().commit();
        logger.log(Level.INFO, "clear(): dropped all categories;");
        return deleted;
    }
}
