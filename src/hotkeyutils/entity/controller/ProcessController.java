/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package hotkeyutils.entity.controller;

import hotkeyutils.entity.Process;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

/**
 *
 * @author Thomas
 */
public class ProcessController {
    
    private EntityManager entityManager = null;
    private EntityManagerFactory entityManagerFactory = null;
    private static final Logger logger = Logger.getLogger("hotkeyLogger");
    
    public ProcessController(){
        if (entityManager == null){
            entityManagerFactory = Persistence.createEntityManagerFactory("hotkey");
            entityManager = entityManagerFactory.createEntityManager();
            logger.log(Level.INFO, "{0}: Entity Manager created;", this.getClass().getName());
        }
    }
    
    public List<Process> getAll(){
        TypedQuery<Process> query = entityManager.createNamedQuery("Process.getAll", Process.class);
        logger.log(Level.INFO, "getAll(): Returning {0} values;", query.getResultList().size());
        return query.getResultList();
    }
    
    public Process getByFilename(String filename){
        Query query = entityManager.createNamedQuery("Process.getByFilename").setParameter("fileName", "%" + filename + "%");
        logger.log(Level.INFO, "getByFilename({0}): was called;", filename);
        return (Process) query.getSingleResult();
    }
    
    public void insert(Process process){
        entityManager.getTransaction().begin();
        entityManager.persist(process);
        entityManager.getTransaction().commit();
        logger.log(Level.INFO, "insert(@Process([Id={0}],[Path={1}])): was called;", new Object[]{process.getId(), process.getPath()});
    }
    
    public int update(Process process){
        entityManager.getTransaction().begin();
        int updated = entityManager.createNamedQuery("Process.update").setParameter("pPath", process.getPath())
                .setParameter("pId", process.getId())
                .executeUpdate();
        entityManager.getTransaction().commit();
        logger.log(Level.INFO, "update(@Process([Id={0}],[Path={1}])): was called;", new Object[]{process.getId(), process.getPath()});
        return updated;
    }
    
    public int clear(){
        entityManager.getTransaction().begin();
        int deleted = entityManager.createQuery("DELETE FROM process").executeUpdate();
        entityManager.createNativeQuery("ALTER TABLE process ALTER COLUMN id RESTART WITH 1").executeUpdate();
        entityManager.getTransaction().commit();
        logger.log(Level.INFO, "clear(): dropped all processes;");
        return deleted;
    }
}
