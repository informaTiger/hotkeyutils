package hotkeyutils.entity;


import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity(name="hotkey")
@Table(name="hotkey")
@NamedQueries({@NamedQuery(name="Hotkey.getAll",query="SELECT h FROM hotkey h"),@NamedQuery(name="Hotkey.getByCategory",query="SELECT h FROM hotkey h JOIN h.category c WHERE c.name = :catName AND h.name = :hkName")})
public  class Hotkey implements Serializable {


    @OneToOne(cascade={CascadeType.REMOVE},targetEntity=Process.class)
    private Process process;


    @Basic
    private String name;


    @Column(unique=true,nullable=false)
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int id;


    @ManyToOne(cascade={CascadeType.PERSIST},targetEntity=Category.class)
    private Category category;

    public Hotkey(){

    }
    
    public Hotkey(String name, Process process){
        this.name = name;
        this.process = process;
    }
    
   public Process getProcess() {
        return this.process;
    }


  public void setProcess (Process process) {
        this.process = process;
    }



   public String getName() {
        return this.name;
    }


  public void setName (String name) {
        this.name = name;
    }



   public int getId() {
        return this.id;
    }


  public void setId (int id) {
        this.id = id;
    }



   public Category getCategory() {
        return this.category;
    }


  public void setCategory (Category category) {
        this.category = category;
    }

}

